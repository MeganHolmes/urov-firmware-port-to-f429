#include "can.h"

/* USER CODE BEGIN 0 */
#include "stdlib.h"
#include "gpio.h"

#define MCU_ID 0x020
#define GUI_ID 0x7E0
#define THRUSTER_PWM_SCALE 0.02555
#define THRUSTER_PWM_OFFSET 1200
#define SERVO_PWM_SCALE 0.06152
#define SERVO_PWM_OFFSET 2700
#define TWO_BYTE_TWOS_COMP_MAX 32767
#define MIN_THRUSTER_PWM 1200
#define MAX_THRUSTER_PWM 2950
#define MIN_SERVO_PWM 2700
#define MAX_SERVO_PWM 6600
#define MEASURED_THRUSTER_0_PWM 2037
#define ACTUAL_THRUSTER_0_PWM 2000
#define MEASURED_SERVO_0_PWM 4715
#define ACTUAL_SERVO_0_PWM 4782
#define TX_DELAY 10 //4

static void convert_float_to_3_bytes(float f, uint8_t* arr);
static uint16_t convert_to_thruster_pwm_scale(uint8_t msb, uint8_t lsb);
static uint16_t convert_to_servo_pwm_scale(uint8_t msb, uint8_t lsb);
static HAL_StatusTypeDef send_packet_of_floats(
		uint8_t packet_num,
		float first_data_pt,
		float second_data_pt,
		CAN_TxHeaderTypeDef* tx_header,
		CAN_HandleTypeDef* hcan1);

CAN_TxHeaderTypeDef setup_tx_header()
{
	CAN_TxHeaderTypeDef txHeader;

	txHeader.StdId = MCU_ID;
	txHeader.IDE = CAN_ID_STD;
	txHeader.RTR = CAN_RTR_DATA;
	txHeader.DLC = 8; // Default


	return txHeader;
}

HAL_StatusTypeDef tx_data(struct sensor_data* data_ptr,
		struct sensor_states* states_ptr,
		CAN_TxHeaderTypeDef* tx_header,
		CAN_HandleTypeDef* hcan1)
{
	if (data_ptr == NULL || states_ptr == NULL || tx_header == NULL)
	{
		error_blink();
		return HAL_ERROR;
	}

	uint8_t txData[8];
	uint8_t packet_num = 0;
	HAL_StatusTypeDef return_val  = HAL_OK;

	// Need to do the first one manually because it sends the statuses
	txData[0] = packet_num;
	packet_num++;
	txData[1] = states_ptr->abs_pressure;
	txData[2] = states_ptr->diff_pressure;
	txData[3] = states_ptr->temperature;
	txData[4] = states_ptr->accel_gyro;
	convert_float_to_3_bytes(data_ptr->abs_pressure, &txData[5]);
	tx_header->DLC = 8;
	HAL_StatusTypeDef temp_return_val = HAL_CAN_AddTxMessage(
					hcan1, tx_header, txData,
					CAN_TX_MAILBOX0);
	CHECK_RETURN_VAL(return_val, temp_return_val);

	tx_header->DLC = 7;

	HAL_Delay(TX_DELAY);
	temp_return_val = send_packet_of_floats(packet_num,
			data_ptr->diff_pressure,
			data_ptr->temperature,
			tx_header,
			hcan1);
	packet_num++;
	CHECK_RETURN_VAL(return_val, temp_return_val);

	HAL_Delay(TX_DELAY);
	temp_return_val = send_packet_of_floats(packet_num,
				data_ptr->temperature_abs_sensor,
				data_ptr->temperature_diff_sensor,
				tx_header,
				hcan1);
	packet_num++;
	CHECK_RETURN_VAL(return_val, temp_return_val);

	HAL_Delay(TX_DELAY);
	temp_return_val = send_packet_of_floats(packet_num,
				data_ptr->tank_percent,
				data_ptr->accel_gyro_temperature,
				tx_header,
				hcan1);
	packet_num++;
	CHECK_RETURN_VAL(return_val, temp_return_val);

	HAL_Delay(TX_DELAY);
	temp_return_val = send_packet_of_floats(packet_num,
				data_ptr->accel_x,
				data_ptr->accel_y,
				tx_header,
				hcan1);
	packet_num++;
	CHECK_RETURN_VAL(return_val, temp_return_val);

	HAL_Delay(TX_DELAY);
	temp_return_val = send_packet_of_floats(packet_num,
				data_ptr->accel_z,
				data_ptr->gyro_x,
				tx_header,
				hcan1);
	packet_num++;
	CHECK_RETURN_VAL(return_val, temp_return_val);

	HAL_Delay(TX_DELAY);
	temp_return_val = send_packet_of_floats(packet_num,
				data_ptr->gyro_y,
				data_ptr->gyro_z,
				tx_header,
				hcan1);
	packet_num++;
	CHECK_RETURN_VAL(return_val, temp_return_val);
	HAL_Delay(TX_DELAY);

	return return_val;
}

HAL_StatusTypeDef rx_data(struct commands* cmds,
		CAN_HandleTypeDef* hcan1)
{

	if (HAL_CAN_GetRxFifoFillLevel(hcan1, CAN_RX_FIFO0) > 0)
	{
		CAN_RxHeaderTypeDef rxHeader;
		uint8_t data[8];

		HAL_StatusTypeDef return_val = HAL_CAN_GetRxMessage(
				hcan1, CAN_RX_FIFO0, &rxHeader, &data);


		if (return_val == HAL_OK)
		{
			if (rxHeader.StdId == GUI_ID)
			{
				cmds->thruster = convert_to_thruster_pwm_scale(data[0], data[1]);
				cmds->elevator = convert_to_servo_pwm_scale(data[2], data[3]);
				cmds->rudder = convert_to_servo_pwm_scale(data[4], data[5]);

				cmds->rudder_drive = data[6];
				cmds->bouyancy_control = data[7];
			}
			else // Else: The packet was corrupted
			{
				return HAL_ERROR;
			}
		}

		return return_val;
	}

	return HAL_OK;
}

void convert_float_to_3_bytes(float fl_in, uint8_t* arr_in)
{
	int exp;
	float normalized_float = frexpf(fl_in, &exp);

	// Process exponential portion
	uint8_t exp_uint = exp < 0 ?
			(uint8_t)(exp*(-1)) | 0x80 :
			(uint8_t)exp;

	// Process float portion
	uint16_t uint_representation = normalized_float < 0 ?
			(uint16_t)(TWO_BYTE_TWOS_COMP_MAX*(normalized_float*-1)) :
			(uint16_t)(TWO_BYTE_TWOS_COMP_MAX*normalized_float);

	uint_representation = normalized_float < 0 ?
			uint_representation | 0x8000 :
			uint_representation;

	// Assign to array
	arr_in[0] = exp_uint;
	arr_in[1] = (uint8_t)(uint_representation >> 8); // Clears lower 8 bits and shifts
	arr_in[2] = (uint8_t)(uint_representation & 0x00FF); // Clears upper 8 bits
}

uint16_t convert_to_thruster_pwm_scale(uint8_t msb, uint8_t lsb)
{
	// 65,535 converts to 2950
	// 32,768 converts to 2000
	// 0 converts to 1200
	uint16_t result = (msb << 8) | lsb;
	result =  (result*THRUSTER_PWM_SCALE) + THRUSTER_PWM_OFFSET;

	result = result < MIN_THRUSTER_PWM ? result = MIN_THRUSTER_PWM : result;
	result = result > MAX_THRUSTER_PWM ? result = MAX_THRUSTER_PWM : result;
	result = result == MEASURED_THRUSTER_0_PWM ?
			result = ACTUAL_THRUSTER_0_PWM : result;

	return result;
}

uint16_t convert_to_servo_pwm_scale(uint8_t msb, uint8_t lsb)
{
	// 65,535 converts to 6600
	// 32,768 converts to 4650
	// 0 converts to 2700
	uint16_t result = (msb << 8) | lsb;
	result =  (result*SERVO_PWM_SCALE) + SERVO_PWM_OFFSET;

	result = result < MIN_SERVO_PWM ? result = MIN_SERVO_PWM : result;
	result = result > MAX_SERVO_PWM ? result = MAX_SERVO_PWM : result;
	result = result == MEASURED_SERVO_0_PWM ?
			result = ACTUAL_SERVO_0_PWM : result;

	return result;
}

static HAL_StatusTypeDef send_packet_of_floats(
		uint8_t packet_num,
		float first_data_pt,
		float second_data_pt,
		CAN_TxHeaderTypeDef* tx_header,
		CAN_HandleTypeDef* hcan1)
{
	uint8_t txData[8] = {0};
	txData[0] = packet_num;

	convert_float_to_3_bytes(first_data_pt, &txData[1]);
	convert_float_to_3_bytes(second_data_pt, &txData[5]);

	return HAL_CAN_AddTxMessage(
				hcan1, tx_header, txData, CAN_TX_MAILBOX0);
}

void send_error_code(uint16_t code, CAN_HandleTypeDef* hcan1)
{
	CAN_TxHeaderTypeDef header = setup_tx_header();
	header.DLC = 2;
	uint8_t txData[2] = {0};
	txData[0] = code >> 8;
	txData[1] = code & 0x00FF;

	HAL_CAN_AddTxMessage(
					hcan1, &header, &txData[0], CAN_TX_MAILBOX0);
	HAL_Delay(TX_DELAY);
}

/* USER CODE END 1 */
