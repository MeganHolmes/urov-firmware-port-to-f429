#ifndef __TIM_H__
#define __TIM_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_tim.h"

/* USER CODE BEGIN Includes */
#include "gpio.h"
#include "sensors.h"

HAL_StatusTypeDef cmd_motors(struct commands* cmds, struct sensor_data* data_ptr,
		TIM_HandleTypeDef* htim1, TIM_HandleTypeDef* htim2, TIM_HandleTypeDef* htim3);
void init_motors(TIM_HandleTypeDef* htim1, TIM_HandleTypeDef* htim3);
void stop_pumps();
void pump_up();

#ifdef __cplusplus
}
#endif
#endif /*__ GPIO_H__ */
