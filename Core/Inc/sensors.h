/*
 * sensors.h
 *
 *  Created on: May 1, 2022
 *      Author: Megan Holmes
 */

#ifndef INC_SENSORS_H_
#define INC_SENSORS_H_

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_i2c.h"
#include "main.h"

/* Defines -------------------------------------------------------------------*/
#define MS5837_30BA_NUM_COEFF 6
#define TSYS01_NUM_COEFF 5
#define NUM_SENSOR_VALS 5
#define NUM_SENSORS 4

/* Structs and Enums ---------------------------------------------------------*/
struct sensor_data
{
	float diff_pressure; // pa
	float abs_pressure; // mbar
	float temperature_diff_sensor; // C
	float temperature_abs_sensor; // C
	float temperature; // C
	float accel_x; // g
	float accel_y; // g
	float accel_z; // g
	float gyro_x; // degrees per second
	float gyro_y; // degrees per second
	float gyro_z; // degrees per second
	float accel_gyro_temperature; // C
	float tank_percent; // %
	uint32_t tank_timer; // ms. Not transmitted to surface.
};

struct sensor_states
{
	HAL_StatusTypeDef diff_pressure;
	HAL_StatusTypeDef abs_pressure;
	HAL_StatusTypeDef temperature;
	HAL_StatusTypeDef accel_gyro;
};

struct sensor_coeffs
{
	uint16_t ms5837_30ba_coeffs[MS5837_30BA_NUM_COEFF]; // 6
	uint16_t tsys01_coeffs[TSYS01_NUM_COEFF]; // 5
};

/* Prototypes ----------------------------------------------------------------*/
HAL_StatusTypeDef init_all_sensors(struct sensor_states* states_ptr,
		struct sensor_coeffs* coeffs, I2C_HandleTypeDef* hi2c1);
HAL_StatusTypeDef read_all_sensors(struct sensor_data* data_ptr,
		struct sensor_states* states_ptr,
		struct sensor_coeffs* coeffs,
		I2C_HandleTypeDef* hi2c1);
HAL_StatusTypeDef is_there_sensor_error(struct sensor_states* states_ptr);
HAL_StatusTypeDef reinit_problem_sensors(struct sensor_states* states_ptr,
		struct sensor_coeffs* coeffs,
		I2C_HandleTypeDef* hi2c1);

uint16_t convert_twos_comp_to_dec(uint16_t);


#endif /* INC_SENSORS_H_ */
