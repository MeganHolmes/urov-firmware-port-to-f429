#include "tim.h"

/* USER CODE BEGIN 0 */
#include <stdlib.h>

#include "sensors.h"

#define MAX_16BIT 65535
#define ESC_INIT_DUTY 2000
#define MIN_PWM_INIT_DELAY_IN_MS 3000
#define MAX_TANK_PERCENT 99.0f
#define TANK_PERCENT_PER_S 7.142
#define TIMER_TO_S 1/4000
#define SERVO_0_DUTY 4782

static void pump_down();
static HAL_StatusTypeDef rudder_control(struct commands* cmds, TIM_HandleTypeDef* htim3);
static HAL_StatusTypeDef buoyancy_control(struct commands* cmds,
		struct sensor_data* data_ptr,
		TIM_HandleTypeDef* htim2);
static void update_tank_percent(float* tank_percent,
		Bouyancy_Direction last_direction,
		uint32_t* last_update,
		TIM_HandleTypeDef* htim2);

HAL_StatusTypeDef cmd_motors(struct commands* cmds, struct sensor_data* data_ptr,
		TIM_HandleTypeDef* htim1, TIM_HandleTypeDef* htim2, TIM_HandleTypeDef* htim3)
{
	__HAL_TIM_SET_COMPARE(htim1, TIM_CHANNEL_1, cmds->thruster);
	__HAL_TIM_SET_COMPARE(htim3, TIM_CHANNEL_1, cmds->elevator);

	HAL_StatusTypeDef return_val = rudder_control(cmds, htim3);

	if (buoyancy_control(cmds, data_ptr, htim2) != HAL_OK)
	{
		error_blink();
		return HAL_ERROR;
	}

	return return_val;
}

void init_motors(TIM_HandleTypeDef* htim1, TIM_HandleTypeDef* htim3)
{
	pump_up();
	// Sends a 1500 us pulse to each channel
	__HAL_TIM_SET_COMPARE(htim1, TIM_CHANNEL_1, ESC_INIT_DUTY);
	__HAL_TIM_SET_COMPARE(htim3, TIM_CHANNEL_1, SERVO_0_DUTY);
	__HAL_TIM_SET_COMPARE(htim3, TIM_CHANNEL_2, SERVO_0_DUTY);
	__HAL_TIM_SET_COMPARE(htim3, TIM_CHANNEL_3, SERVO_0_DUTY);
	HAL_Delay(MIN_PWM_INIT_DELAY_IN_MS); // Needs to be at least this long between the
										 // first pulse and subsequent actions.
	stop_pumps();

	// Might need to do more for the pumps.
}

void stop_pumps()
{
	 HAL_GPIO_WritePin(Pump_In_GPIO_Port, Pump_In_Pin,  GPIO_PIN_RESET);
	 HAL_GPIO_WritePin(Pump_Out_GPIO_Port, Pump_Out_Pin,  GPIO_PIN_RESET);
}

void pump_up()
{
	 HAL_GPIO_WritePin(Pump_In_GPIO_Port, Pump_In_Pin,  GPIO_PIN_RESET);
	 HAL_GPIO_WritePin(Pump_Out_GPIO_Port, Pump_Out_Pin,  GPIO_PIN_SET);
}

static void pump_down()
{
	HAL_GPIO_WritePin(Pump_In_GPIO_Port, Pump_In_Pin,  GPIO_PIN_SET);
 	HAL_GPIO_WritePin(Pump_Out_GPIO_Port, Pump_Out_Pin,  GPIO_PIN_RESET);
}

static HAL_StatusTypeDef rudder_control(struct commands* cmds, TIM_HandleTypeDef* htim3)
{
	__HAL_TIM_SET_COMPARE(htim3, TIM_CHANNEL_2, cmds->rudder);

	if (cmds->rudder_drive == RUDDER_DRIVE_TOGETHER)
	{
		__HAL_TIM_SET_COMPARE(htim3, TIM_CHANNEL_3, cmds->rudder);
	}
	else if (cmds->rudder_drive == RUDDER_DRIVE_DIFF)
	{
		uint16_t offset_from_centre = abs(cmds->rudder - SERVO_0_DUTY/2);

		uint16_t diff_rudder = cmds->rudder > SERVO_0_DUTY/2 ?
				cmds->rudder - offset_from_centre : cmds->rudder + offset_from_centre;

		__HAL_TIM_SET_COMPARE(htim3, TIM_CHANNEL_3, diff_rudder);
	}
	else
	{
		error_blink();
		return HAL_ERROR;
	}

	return HAL_OK;
}

// TODO: This function needs a review
static HAL_StatusTypeDef buoyancy_control(struct commands* cmds,
		struct sensor_data* data_ptr,
		TIM_HandleTypeDef* htim2)
{
//	HAL_StatusTypeDef return_val = HAL_OK;
//
//	// Check & update the tank capacity
//	update_tank_percent(&data_ptr->tank_percent,
//			cmds->last_bouyancy_control,
//			&data_ptr->tank_timer,
//			htim2);
//
//	// If the pumps stop, stop the timer
//	// If the pumps change direction, restart the timer from 0
//	// If the tank is full, stop the pumps and the timer
//
//	if (cmds->bouyancy_control == Bouyancy_Direction_HOLD)
//	{
//		stop_pumps();
//		cmds->last_bouyancy_control = Bouyancy_Direction_HOLD;
//		data_ptr->tank_timer = 0;
//	}
//	else if (cmds->bouyancy_control == Bouyancy_Direction_DOWN) // Command downwards
//	{
//		if (data_ptr->tank_percent < MAX_TANK_PERCENT) // If the tank is not full
//		{
//			if (cmds->last_bouyancy_control != Bouyancy_Direction_DOWN)
//			{
//				// Restart the timer since we are now going different direction.
//				if (restart_buoyancy_timer() == HAL_OK) // If the timer successfully starts
//				{
//					data_ptr->tank_timer = 0;
//					pump_down();
//				}
//				else // The timer did not start correctly, we can't know how full the tank is
//				{
//					stop_pumps_and_timer();
//					data_ptr->tank_timer = 0;
//					error_blink();
//					return_val = HAL_ERROR;
//				}
//			}
//		}
//		else // The tank is full
//		{
//			 stop_pumps();
//		}
//
//		cmds->last_bouyancy_control = Bouyancy_Direction_DOWN;
//	}
//	else
//	{
//		// This one is the default because if it's not one of the other two then we
//		// either intentionally want to surface or there is an error and we still
//		// want to surface.
//		 pump_up();
//
//		 if (cmds->last_bouyancy_control != Bouyancy_Direction_UP)
//		 {
//			// Restart the timer since we are now going different direction.
//
//			 // This isn't a condition for pump_up like it is for down because
//			 // Even if the timer fails, we still want to run the pump out to surface
//			 return_val = restart_buoyancy_timer();
//		 }
//
//		 cmds->last_bouyancy_control = Bouyancy_Direction_UP;
//
//		 if (cmds->bouyancy_control != Bouyancy_Direction_UP)
//		 {
//			 // If it was an error, return that.
//			 return_val = HAL_ERROR;
//		 }
//	}
//
//	return return_val;
}

static void update_tank_percent(float* tank_percent,
		Bouyancy_Direction last_direction,
		uint32_t* last_update,
		TIM_HandleTypeDef* htim2)
{
	// Get current time (microseconds)
	uint32_t timer_val = __HAL_TIM_GET_COUNTER(htim2);
	uint32_t delta_time = timer_val > *last_update ? timer_val - *last_update :
			0;
	*last_update = timer_val;

	float delta_tank_percent = (float)delta_time*TANK_PERCENT_PER_S*TIMER_TO_S;

	*tank_percent += last_direction == Bouyancy_Direction_DOWN ?
			delta_tank_percent : -delta_tank_percent;

	if (*tank_percent >= MAX_TANK_PERCENT)
	{
		stop_pumps();
	}
	else if (*tank_percent <= 0)
	{
		// Even if the timer fails, we still want to run the pump out to surface
//		*tank_percent = 0;
	}
}
