/*
 * sensors.c
 *
 *  Created on: May 1, 2022
 *      Author: Megan Holmes
 */

/* Includes ------------------------------------------------------------------*/
#include <stdlib.h>

#include "sensors.h"
#include "gpio.h"
#include "main.h"

/* Private Defines -----------------------------------------------------------*/
#define I2C_TIMEOUT_IN_MS 35 // ms
#define TWO_TO_THE_EIGHT 256
#define TWO_TO_THE_TWENTY_THREE 8388608
#define TWO_TO_THE_FIFTEEN 32768
#define TWO_TO_THE_TWENTY_ONE 2097152
#define TWO_TO_THE_THIRTEEN 8192
#define TWO_TO_THE_SIXTEEN 65536
#define TWO_TO_THE_SEVEN 128
#define TWO_TO_THE_FOURTEEN 16384

// Differential Pressure Sensor SEN0343
#define SEN0343_ADDRESS 0x00
#define SEN0343_CONFIG_REG 0xAA
#define SEN0343_CONFIG_REG_SIZE 2 // bytes
#define SEN0343_I2C_CONVERSION_DELAY_IN_MS 30 // ms
#define SEN0343_DATA_SIZE_IN_BYTES 6 // bytes
#define SEN0343_TEMP_BIAS -40.0f // C
#define SENO343_MAX_TEMPERATURE_VALUE 65 // C
#define SENO343_MIN_TEMPERATURE_VALUE -5 // C
#define SENO343_MAX_PRESSURE_VALUE 500.0f // pa
#define SENO343_MIN_PRESSURE_VALUE -500.0f // pa
#define SEN0343_PRESSURE_MAX_CALC SENO343_MAX_PRESSURE_VALUE
#define SEN0343_PRESSURE_MIN_CALC SENO343_MIN_PRESSURE_VALUE

// Absolute Pressure Sensor MS5837-30BA
#define MS5837_30BA_ADDRESS 0x76
#define MS5837_30BA_ADC_READ_CMD 0x00
#define MS5837_30BA_COEFF_CMD_0 0xA2 // Goes from A0 to AC, 6 coeffs
#define MS5837_30BA_PRESSURE_CMD 0X48
#define MS5837_30BA_TEMPERATURE_CMD 0x58
#define MS5837_30BA_I2C_CONVERSION_DELAY_IN_MS 18.08 // ms
#define MS5837_30BA_DATA_SIZE_IN_BYTES 3 // bytes
#define MS5837_30BA_COEFF_SIZE_IN_BYTES 2 // bytes
#define MS5837_30BA_MAX_TEMPERATURE_VALUE 85 // C
#define MS5837_30BA_MIN_TEMPERATURE_VALUE -20 // C
#define MS5837_30BA_MAX_PRESSURE_VALUE 300000 // mbar
#define MS5837_30BA_MIN_PRESSURE_VALUE 0 // mbar
#define MS5837_30BA_RESET_CMD 0X1E

// Temperature Sensor TSYS01
#define TSYS01_ADDRESS 0x77
#define TSYS01_ADC_CONVERSION 0x48
#define TSYS01_ADC_READ_CMD 0x00
#define TSYS01_COEFF_CMD_0 0xAA
#define TSYS01_I2C_CONVERSION_DELAY_IN_MS 9.04 // ms
#define TSYS01_DATA_SIZE_IN_BYTES 3 // bytes
#define TSYS01_COEFF_SIZE_IN_BYTES 2 // bytes
#define TSYS01_MAX_VALUE 125 // C
#define TSYS01_MIN_VALUE -40 // C
#define TSYS01_RESET_TIME_IN_MS 2 // ms
#define TSYS01_RESET_CMD 0X1E

// Accel / Gyro Sensor LSM6DS3
#define LSM6DS3_ADDRESS 0x6A // This might be 0x69, 0x6B or 0x6A
#define LSM6DS3_ACCEL_CTRL_REG 0x10
#define LSM6DS3_GYRO_CTRL_REG 0x11
#define LSM6DS3_CTRL_REG 0X13
#define LSM6DS3_READ_REG_0 0X20 // Goes to 0x2D
#define LSM6DS3_ACCEL_SET 0xA0 // Sets for full-scale 400hz AA, and 6.66khz sample
#define LSM6DS3_GYRO_SET 0xA0 // Sets for full-scale 400hz AA, and 6.66khz sample
#define LSM6DS3_CTRL_SET 0x10 // Enables temperature sensor
#define LSM6DS3_NUM_INIT_CMDS 3
#define LSM6DS3_NUM_DATA_PTS 7
#define LSM6DS3_ACCEL_PER_BIT 0.061 // mg/LSB
#define LSM6DS3_ANGLE_PER_BIT 4.375 // mdps/LSB
#define LSM6DS3_TEMPERATURE_OFFSET 25 // C
#define LSM6DS3_MAX_ACCEL 2
#define LSM6DS3_MAX_GYRO 180
#define LSM6DS3_MAX_TEMPERATURE 40
#define LSM6DS3_MIN_TMPERATURE 0
#define LSM6DS3_TEMPERATURE_PER_BIT 1/40 // C/LSB. Datasheet says 1/16, I have
										 // adjusted it until it matched TSYS01

/* Private Functions -------------------------------------------------------- */
static HAL_StatusTypeDef read_temperature_and_pressure_from_diff_pressure_sensor(
		float* temperature_ptr,
		float* pressure_ptr,
		I2C_HandleTypeDef* hi2c1);
static HAL_StatusTypeDef read_temperature_and_pressure_from_abs_pressure_sensor(
		float* temperature_ptr,
		float* pressure_ptr,
		struct sensor_coeffs* coeffs,
		I2C_HandleTypeDef* hi2c1);
static HAL_StatusTypeDef read_from_temperature_sensor(
		float* temperature_ptr,
		struct sensor_coeffs* coeffs,
		I2C_HandleTypeDef* hi2c1);
static HAL_StatusTypeDef read_from_accel_gyro_sensor(
		struct sensor_data* data_ptr,
		I2C_HandleTypeDef* hi2c1);

static HAL_StatusTypeDef read_from_diff_pressure_sensor_with_i2c(
		uint8_t* data_ptr,
		I2C_HandleTypeDef* hi2c1);
static HAL_StatusTypeDef read_from_abs_pressure_sensor_with_i2c(
		uint8_t* data_ptr, uint8_t command,
		I2C_HandleTypeDef* hi2c1);
static HAL_StatusTypeDef read_from_temperature_sensor_with_i2c(
		uint8_t* data_ptr,
		I2C_HandleTypeDef* hi2c1);
static HAL_StatusTypeDef read_from_accel_gyro_sensor_with_i2c(
		uint8_t* data_ptr,
		I2C_HandleTypeDef* hi2c1);

static HAL_StatusTypeDef reset_abs_pressure_sensor(I2C_HandleTypeDef* hi2c1);
static HAL_StatusTypeDef reset_temperature_sensor(I2C_HandleTypeDef* hi2c1);

static HAL_StatusTypeDef init_abs_pressure_sensor(struct sensor_coeffs* coeffs,
		I2C_HandleTypeDef* hi2c1);
static HAL_StatusTypeDef init_temperature_sensor(struct sensor_coeffs* coeffs,
		I2C_HandleTypeDef* hi2c1);
static HAL_StatusTypeDef init_accel_gyro_sensor(I2C_HandleTypeDef* hi2c1);

static HAL_StatusTypeDef get_abs_pressure_sensor_coeffs(struct sensor_coeffs* coeffs,
		I2C_HandleTypeDef* hi2c1);
static HAL_StatusTypeDef get_temperature_sensor_coeffs(struct sensor_coeffs* coeffs,
		I2C_HandleTypeDef* hi2c1);

static HAL_StatusTypeDef check_accel_gyro_data(struct sensor_data* data_ptr);


/* Functions -----------------------------------------------------------------*/

// Initializes all sensors by calling sub-functions
HAL_StatusTypeDef init_all_sensors(struct sensor_states* states_ptr,
		struct sensor_coeffs* coeffs, I2C_HandleTypeDef* hi2c1)
{
	if (states_ptr == NULL || coeffs == NULL)
	{
		error_blink();
		return HAL_ERROR;
	}

	HAL_StatusTypeDef return_val = HAL_OK;
	HAL_StatusTypeDef temp_return_val;

	// Differential sensor doesn't need init
	states_ptr->diff_pressure = HAL_OK;

	// Absolute sensor
	temp_return_val = init_abs_pressure_sensor(coeffs, hi2c1);
	states_ptr->abs_pressure = temp_return_val;

	CHECK_RETURN_VAL(return_val, temp_return_val);

	// Temperature sensor
	temp_return_val = init_temperature_sensor(coeffs, hi2c1);
	states_ptr->temperature = temp_return_val;

	CHECK_RETURN_VAL(return_val, temp_return_val);

	// Accel + Gyro sensor

	temp_return_val = init_accel_gyro_sensor(hi2c1);
	states_ptr->accel_gyro = temp_return_val;

	CHECK_RETURN_VAL(return_val, temp_return_val);

	return return_val;
}

static HAL_StatusTypeDef reset_temperature_sensor(I2C_HandleTypeDef* hi2c1)
{
	uint8_t tsys01_reset = TSYS01_RESET_CMD;

	return HAL_I2C_Master_Transmit(hi2c1,
			TSYS01_ADDRESS<<1,
			&tsys01_reset, 1,
			I2C_TIMEOUT_IN_MS);
}

// Resets the ms5837_30ba pressure sensor
static HAL_StatusTypeDef reset_abs_pressure_sensor(I2C_HandleTypeDef* hi2c1)
{
	uint8_t ms5837_30ba_reset = MS5837_30BA_RESET_CMD ;

	return HAL_I2C_Master_Transmit(hi2c1,
			MS5837_30BA_ADDRESS<<1,
			&ms5837_30ba_reset, 1,
			I2C_TIMEOUT_IN_MS);
}

static HAL_StatusTypeDef init_accel_gyro_sensor(I2C_HandleTypeDef* hi2c1)
{
	uint8_t registers[LSM6DS3_NUM_INIT_CMDS] = {LSM6DS3_CTRL_REG,
			LSM6DS3_ACCEL_CTRL_REG, LSM6DS3_GYRO_CTRL_REG};
	uint8_t commands[LSM6DS3_NUM_INIT_CMDS] = {LSM6DS3_CTRL_SET,
			LSM6DS3_ACCEL_SET, LSM6DS3_GYRO_SET};

	for (int cmd_num = 0; cmd_num < LSM6DS3_NUM_INIT_CMDS; cmd_num++)
	{
		HAL_StatusTypeDef return_val = HAL_I2C_Mem_Write(hi2c1, LSM6DS3_ADDRESS << 1,
				registers[cmd_num], 1,
				&commands[cmd_num], 1,
				I2C_TIMEOUT_IN_MS);

		if (return_val != HAL_OK)
		{
			error_blink();
			return return_val;
		}
	}

	return HAL_OK;
}

static HAL_StatusTypeDef init_temperature_sensor(struct sensor_coeffs* coeffs,
		I2C_HandleTypeDef* hi2c1)
{
	if (coeffs == NULL)
	{
		error_blink();
		return HAL_ERROR;
	}

	HAL_StatusTypeDef return_val;

	return_val = reset_temperature_sensor(hi2c1);

	if (return_val != HAL_OK)
	{
		error_blink();
		return return_val; // If the reset failed there is no point to get the coeffs
	}

	HAL_Delay(TSYS01_RESET_TIME_IN_MS);

	return get_temperature_sensor_coeffs(coeffs, hi2c1);
}

static HAL_StatusTypeDef init_abs_pressure_sensor(struct sensor_coeffs* coeffs,
		I2C_HandleTypeDef* hi2c1)
{
	if (coeffs == NULL)
	{
		error_blink();
		return HAL_ERROR;
	}

	HAL_StatusTypeDef return_val;

	return_val = reset_abs_pressure_sensor(hi2c1);

	if (return_val != HAL_OK)
	{
		error_blink();
		return return_val; // If the reset failed there is no point to get the coeffs
	}

	return get_abs_pressure_sensor_coeffs(coeffs, hi2c1);
}

static HAL_StatusTypeDef get_temperature_sensor_coeffs(struct sensor_coeffs* coeffs,
		I2C_HandleTypeDef* hi2c1)
{
	if (coeffs == NULL)
	{
		error_blink();
		return HAL_ERROR;
	}

	HAL_StatusTypeDef return_val = HAL_OK;
	HAL_StatusTypeDef temp_return_val;
	uint8_t tsys01_coeff = TSYS01_COEFF_CMD_0;
	uint8_t coeff_bytes[TSYS01_COEFF_SIZE_IN_BYTES];

	for (int coeff_idx = 0; coeff_idx < TSYS01_NUM_COEFF; coeff_idx++)
	{
		// Send command to ask the device for a coefficient. device loads coefficient into
		// read register
		temp_return_val = HAL_I2C_Master_Transmit(hi2c1,
				TSYS01_ADDRESS << 1,
				&tsys01_coeff, 1,
				I2C_TIMEOUT_IN_MS);

		// We don't exit the loop on a failure because some coefficients are better
		// than none
		CHECK_RETURN_VAL(return_val, temp_return_val);

		// read the register where the coefficient is now located.
		if (temp_return_val == HAL_OK)
		{
			temp_return_val = HAL_I2C_Master_Receive(hi2c1,
					TSYS01_ADDRESS << 1,
					coeff_bytes, TSYS01_COEFF_SIZE_IN_BYTES,
					I2C_TIMEOUT_IN_MS);

			CHECK_RETURN_VAL(return_val, temp_return_val);

			// Add the bytes together. MSB first
			coeffs->tsys01_coeffs[coeff_idx] = (coeff_bytes[0] << 8) + coeff_bytes[1];
		}

		// Last value of address is always 0 so need to increment by 2
		tsys01_coeff -= 2;
	}

	return return_val;
}

static HAL_StatusTypeDef get_abs_pressure_sensor_coeffs(struct sensor_coeffs* coeffs,
		I2C_HandleTypeDef* hi2c1)
{
	if (coeffs == NULL)
	{
		error_blink();
		return HAL_ERROR;
	}

	HAL_StatusTypeDef return_val = HAL_OK;
	HAL_StatusTypeDef temp_return_val;
	uint8_t ms5837_30ba_coeff = MS5837_30BA_COEFF_CMD_0;
	uint8_t coeff_bytes[MS5837_30BA_COEFF_SIZE_IN_BYTES];

	for (int coeff_idx = 0; coeff_idx < MS5837_30BA_NUM_COEFF; coeff_idx++)
	{
		// Send command to ask the device for a coefficient. device loads coefficient into
		// read register
		temp_return_val = HAL_I2C_Master_Transmit(hi2c1,
				MS5837_30BA_ADDRESS << 1,
				&ms5837_30ba_coeff, 1,
				I2C_TIMEOUT_IN_MS);

		// We don't exit the loop on a failure because some coefficients are better
		// than none
		CHECK_RETURN_VAL(return_val, temp_return_val);

		// read the register where the coefficient is now located.
		temp_return_val = HAL_I2C_Master_Receive(hi2c1,
				MS5837_30BA_ADDRESS << 1,
				coeff_bytes, MS5837_30BA_COEFF_SIZE_IN_BYTES,
				I2C_TIMEOUT_IN_MS);

		CHECK_RETURN_VAL(return_val, temp_return_val);

		// Add the bytes together. MSB first
		coeffs->ms5837_30ba_coeffs[coeff_idx] = (coeff_bytes[0] << 8) + coeff_bytes[1];

		// Last value of address is always 0 so need to increment by 2
		ms5837_30ba_coeff += 2;
	}

	return return_val;
}

// Reads data from all sensors by calling sub-functions
HAL_StatusTypeDef read_all_sensors(struct sensor_data* data_ptr,
		struct sensor_states* states_ptr,
		struct sensor_coeffs* coeffs,
		I2C_HandleTypeDef* hi2c1)
{
	if (coeffs == NULL || data_ptr == NULL || states_ptr == NULL)
	{
		error_blink();
		return HAL_ERROR;
	}

	HAL_StatusTypeDef return_val = HAL_OK;
	HAL_StatusTypeDef temp_return_val;

	if (states_ptr->diff_pressure == HAL_OK)
	{
		temp_return_val = read_temperature_and_pressure_from_diff_pressure_sensor(
				&data_ptr->temperature_diff_sensor,
				&data_ptr->diff_pressure,
				hi2c1);

		states_ptr->diff_pressure = temp_return_val;
		CHECK_RETURN_VAL(return_val, temp_return_val);
	}
	else
	{
		error_blink();
		return_val = states_ptr->diff_pressure;
	}

	if (states_ptr->abs_pressure == HAL_OK)
	{
		temp_return_val = read_temperature_and_pressure_from_abs_pressure_sensor(
				&data_ptr->temperature_abs_sensor,
				&data_ptr->abs_pressure,
				coeffs,
				hi2c1);

		states_ptr->abs_pressure = temp_return_val;
		CHECK_RETURN_VAL(return_val, temp_return_val);
	}
	else
	{
		error_blink();
		return_val = states_ptr->abs_pressure;
	}

	if (states_ptr->temperature == HAL_OK)
	{
		temp_return_val = read_from_temperature_sensor(
				&data_ptr->temperature,
				coeffs,
				hi2c1);

		states_ptr->temperature = temp_return_val;
		CHECK_RETURN_VAL(return_val, temp_return_val);
	}
	else
	{
		error_blink();
		return_val = states_ptr->temperature;
	}

	if (states_ptr->accel_gyro == HAL_OK)
	{
		temp_return_val = read_from_accel_gyro_sensor(data_ptr, hi2c1);

		states_ptr->accel_gyro = temp_return_val;
		CHECK_RETURN_VAL(return_val, temp_return_val);
	}
	else
	{
		error_blink();
		return_val = states_ptr->accel_gyro;
	}

	return return_val;
}

static HAL_StatusTypeDef read_from_accel_gyro_sensor(
		struct sensor_data* data_ptr,
		I2C_HandleTypeDef* hi2c1)
{
	uint8_t raw_data[LSM6DS3_NUM_DATA_PTS*2] = {0};

	HAL_StatusTypeDef return_val = read_from_accel_gyro_sensor_with_i2c(raw_data, hi2c1);

	if (return_val == HAL_OK)
	{
		// Each reading is 16 bits in 2's complement. sets of two bytes where first byte is
		// lower half and second byte is upper half.
		// 1st set: temperature
		// 2nd set: gyro x
		// 3rd set: gyro y
		// 4th set: gyro z
		// 5-7 sets: accel x/y/z
		int16_t unscaled_data[LSM6DS3_NUM_DATA_PTS] = {0};

		for (int data_num = 0; data_num < LSM6DS3_NUM_DATA_PTS; data_num++)
		{
			// Take the two seperate bytes and convert to 2 bytes. LSB is first byte
			 unscaled_data[data_num] = (int16_t)(raw_data[data_num*2]) |
					 (((int16_t)(raw_data[(data_num*2)+1])) << 8);

			 unscaled_data[data_num] = convert_twos_comp_to_dec(unscaled_data[data_num]);
		}

		data_ptr->accel_gyro_temperature =
				-(float)unscaled_data[0]*LSM6DS3_TEMPERATURE_PER_BIT
				+LSM6DS3_TEMPERATURE_OFFSET;
		data_ptr->gyro_x = (float)unscaled_data[1]*LSM6DS3_ANGLE_PER_BIT/1000;
		data_ptr->gyro_y = (float)unscaled_data[2]*LSM6DS3_ANGLE_PER_BIT/1000;
		data_ptr->gyro_z = (float)unscaled_data[3]*LSM6DS3_ANGLE_PER_BIT/1000;
		data_ptr->accel_x = (float)unscaled_data[4]*LSM6DS3_ACCEL_PER_BIT/1000;
		data_ptr->accel_y = (float)unscaled_data[5]*LSM6DS3_ACCEL_PER_BIT/1000;
		data_ptr->accel_z = (float)unscaled_data[6]*LSM6DS3_ACCEL_PER_BIT/1000;

			// TODO: fix this
//		return_val = check_accel_gyro_data(data_ptr);
	}
	else
	{
		error_blink();
	}

	return return_val;
}

static HAL_StatusTypeDef read_from_temperature_sensor(
		float* temperature_ptr,
		struct sensor_coeffs* coeffs,
		I2C_HandleTypeDef* hi2c1)
{
	if (temperature_ptr == NULL || coeffs == NULL)
	{
		error_blink();
		return HAL_ERROR;
	}

	uint8_t raw_data[TSYS01_DATA_SIZE_IN_BYTES];

	HAL_StatusTypeDef return_val = read_from_temperature_sensor_with_i2c(raw_data, hi2c1);

	if (return_val == HAL_OK)
	{
		uint32_t raw_temp;

		// Sensor returns MSB first
		raw_temp = (raw_data[0] << (2*8)) + (raw_data[1] << 8) + raw_data[2];

		// Conversion equations from datasheet
		float adc16 = raw_temp / 256.0f;

		*temperature_ptr = -2.0f * coeffs->tsys01_coeffs[4] * pow(10,-21) * pow(adc16,4)
						+ 4.0f * coeffs->tsys01_coeffs[3] * pow(10,-16) * pow(adc16,3)
						- 2.0f * coeffs->tsys01_coeffs[2] * pow(10,-11) * pow(adc16,2)
						+ 1.0f * coeffs->tsys01_coeffs[1] * pow(10,-6) * adc16
						- 1.5f * coeffs->tsys01_coeffs[0] * pow(10,-2);

		if (*temperature_ptr > TSYS01_MAX_VALUE || *temperature_ptr < TSYS01_MIN_VALUE)
		{
			error_blink();
			return HAL_ERROR;
		}
	}
	else
	{
		error_blink();
	}

	return return_val;
}

// Calls a subfunction to access data via I2C then performs conversion
static HAL_StatusTypeDef read_temperature_and_pressure_from_diff_pressure_sensor(
		float* temperature_ptr,
		float* pressure_ptr,
		I2C_HandleTypeDef* hi2c1)
{
	if (temperature_ptr == NULL || pressure_ptr == NULL)
	{
		error_blink();
		return HAL_ERROR;
	}

	uint8_t raw_data[SEN0343_DATA_SIZE_IN_BYTES];
	HAL_StatusTypeDef return_val = HAL_OK;

	return_val = read_from_diff_pressure_sensor_with_i2c(raw_data, hi2c1);

	if (return_val == HAL_OK)
	{
		uint32_t raw_temp;
		uint32_t raw_pressure;

		// Sensor returns MSB first. first 3 bytes are pressure, last 3 are temperature

		// The first bit of the MSB for both temp and pressure is useless.

		raw_pressure = (raw_data[0] << (2*8)) + (raw_data[1] << 8) + raw_data[2];
		raw_temp = (raw_data[3] << (2*8)) + (raw_data[4] << 8) + raw_data[5];

		// Conversion equations from datasheet
		*temperature_ptr = (((85.0f + 40.0f)/TWO_TO_THE_SIXTEEN)*raw_temp)+ SEN0343_TEMP_BIAS;
		*pressure_ptr = (((SEN0343_PRESSURE_MAX_CALC - SEN0343_PRESSURE_MIN_CALC)
				/TWO_TO_THE_FOURTEEN)*raw_pressure) + SEN0343_PRESSURE_MIN_CALC;

		// TODO: Fix this
//		if (*temperature_ptr > SENO343_MAX_TEMPERATURE_VALUE ||
//				*temperature_ptr < SENO343_MIN_TEMPERATURE_VALUE ||
//				*pressure_ptr > SENO343_MAX_PRESSURE_VALUE ||
//				*pressure_ptr < SENO343_MIN_PRESSURE_VALUE)
//		{
//			error_blink();
//			return HAL_ERROR;
//		}
	}
	else
	{
		error_blink();
	}

	return return_val;
}

static HAL_StatusTypeDef read_temperature_and_pressure_from_abs_pressure_sensor(
		float* temperature_ptr,
		float* pressure_ptr,
		struct sensor_coeffs* coeffs,
		I2C_HandleTypeDef* hi2c1)
{
	if (temperature_ptr == NULL || pressure_ptr == NULL || coeffs == NULL)
	{
		error_blink();
		return HAL_ERROR;
	}

	uint8_t raw_pressure_data[MS5837_30BA_DATA_SIZE_IN_BYTES];
	uint8_t raw_temperature_data[MS5837_30BA_DATA_SIZE_IN_BYTES];
	HAL_StatusTypeDef return_val = HAL_OK;

	return_val = read_from_abs_pressure_sensor_with_i2c(raw_pressure_data,
			MS5837_30BA_PRESSURE_CMD, hi2c1);

	if (return_val == HAL_OK)
	{
		return_val = read_from_abs_pressure_sensor_with_i2c(raw_temperature_data,
				MS5837_30BA_TEMPERATURE_CMD, hi2c1);

		if (return_val != HAL_OK)
		{
			error_blink();
			return return_val;
		}
	}
	else
	{
		error_blink();
		return return_val;
	}

	uint32_t raw_pressure = (raw_pressure_data[0] << (2*8))
							+ (raw_pressure_data[1] << 8)
							+ raw_pressure_data[2];
	uint32_t raw_temperature = (raw_temperature_data[0] << (2*8))
							+ (raw_temperature_data[1] << 8)
							+ raw_temperature_data[2];

	int delta_t = raw_temperature - (coeffs->ms5837_30ba_coeffs[4] * TWO_TO_THE_EIGHT);
	*temperature_ptr = (2000.0f
			+ (delta_t *coeffs->ms5837_30ba_coeffs[5] / TWO_TO_THE_TWENTY_THREE))
					/ 100.0f;

	long offset = coeffs->ms5837_30ba_coeffs[1] * TWO_TO_THE_SIXTEEN
			+ (coeffs->ms5837_30ba_coeffs[3] * delta_t) / TWO_TO_THE_SEVEN;

	long sensitivity = coeffs->ms5837_30ba_coeffs[0] * TWO_TO_THE_FIFTEEN
			+ (coeffs->ms5837_30ba_coeffs[2] * delta_t) / TWO_TO_THE_EIGHT;

	*pressure_ptr = ((((float)raw_pressure * sensitivity / TWO_TO_THE_TWENTY_ONE) - offset)
			/ TWO_TO_THE_THIRTEEN) / 10.0f;

	if (*temperature_ptr > MS5837_30BA_MAX_TEMPERATURE_VALUE ||
			*temperature_ptr < MS5837_30BA_MIN_TEMPERATURE_VALUE ||
			*pressure_ptr > MS5837_30BA_MAX_PRESSURE_VALUE ||
			*pressure_ptr < MS5837_30BA_MIN_PRESSURE_VALUE)
	{
		error_blink();
		return HAL_ERROR;
	}

	return return_val;
}

static HAL_StatusTypeDef read_from_accel_gyro_sensor_with_i2c(
		uint8_t* data_ptr,
		I2C_HandleTypeDef* hi2c1)
{
	uint8_t reg = LSM6DS3_READ_REG_0;
	uint8_t size = 1;
	HAL_StatusTypeDef return_val = HAL_OK;

	for (int cmd_num = 0; cmd_num < LSM6DS3_NUM_DATA_PTS*2; cmd_num++)
	{
		HAL_StatusTypeDef temp_return_val = HAL_I2C_Mem_Read(hi2c1,
				LSM6DS3_ADDRESS << 1,
				reg, 1,
				data_ptr + cmd_num, size,
				I2C_TIMEOUT_IN_MS);

		CHECK_RETURN_VAL(return_val, temp_return_val);

		reg++;
	}

	return return_val;
}

static HAL_StatusTypeDef read_from_temperature_sensor_with_i2c(
		uint8_t* data_ptr,
		I2C_HandleTypeDef* hi2c1)
{
	if (data_ptr == NULL)
	{
		error_blink();
		return HAL_ERROR;
	}

	HAL_StatusTypeDef return_val = HAL_OK;
	uint8_t command = TSYS01_ADC_CONVERSION;

	// Command the sensor to process a reading through the ADC
	return_val = HAL_I2C_Master_Transmit(hi2c1,
			TSYS01_ADDRESS<<1,
			&command, 1,
			I2C_TIMEOUT_IN_MS);

	if (return_val != HAL_OK)
	{
		error_blink();
		return return_val;
	}

	command = TSYS01_ADC_READ_CMD;

	HAL_Delay(TSYS01_I2C_CONVERSION_DELAY_IN_MS);

	return_val = HAL_I2C_Master_Transmit(hi2c1,
			TSYS01_ADDRESS<<1,
			&command, 1,
			I2C_TIMEOUT_IN_MS);

	if (return_val != HAL_OK)
	{
		error_blink();
		return return_val;
	}

	return HAL_I2C_Master_Receive(hi2c1,
			TSYS01_ADDRESS<<1,
			data_ptr, TSYS01_DATA_SIZE_IN_BYTES,
			I2C_TIMEOUT_IN_MS);

}

// Sends commands to retrieve data from sen0343
static HAL_StatusTypeDef read_from_diff_pressure_sensor_with_i2c(
		uint8_t* data_ptr,
		I2C_HandleTypeDef* hi2c1)
{
	if (data_ptr == NULL)
	{
		error_blink();
		return HAL_ERROR;
	}

	uint8_t* sen0343_config_bits = malloc(SEN0343_CONFIG_REG_SIZE);
	sen0343_config_bits[0] = 0x00;
	sen0343_config_bits[1] = 0x80;

	HAL_StatusTypeDef return_val = HAL_OK;

	// Command the sensor to process a reading through the ADC
	return_val = HAL_I2C_Mem_Write(hi2c1, SEN0343_ADDRESS << 1,
			SEN0343_CONFIG_REG, 1,
			sen0343_config_bits, SEN0343_CONFIG_REG_SIZE,
			I2C_TIMEOUT_IN_MS);

	if (return_val != HAL_OK)
	{
		error_blink();
		return return_val; // If the command to read failed there is no point to receive
	}

	HAL_Delay(SEN0343_I2C_CONVERSION_DELAY_IN_MS);

	// Retrieve converted data
	return HAL_I2C_Master_Receive(hi2c1,
			SEN0343_ADDRESS << 1,
			data_ptr, SEN0343_DATA_SIZE_IN_BYTES,
			I2C_TIMEOUT_IN_MS);
}

static HAL_StatusTypeDef read_from_abs_pressure_sensor_with_i2c(
		uint8_t* data_ptr, uint8_t command,
		I2C_HandleTypeDef* hi2c1)
{
	if (data_ptr == NULL)
	{
		error_blink();
		return HAL_ERROR;
	}

	HAL_StatusTypeDef return_val = HAL_OK;

	// Command the sensor to process a reading through the ADC
	return_val = HAL_I2C_Master_Transmit(hi2c1,
			MS5837_30BA_ADDRESS<<1,
			&command, 1,
			I2C_TIMEOUT_IN_MS);

	if (return_val != HAL_OK)
	{
		error_blink();
		return return_val; // If the command to read failed there is no point to receive
	}

	HAL_Delay(MS5837_30BA_I2C_CONVERSION_DELAY_IN_MS);

	command = MS5837_30BA_ADC_READ_CMD;

	//	 Retrieve converted data
	return_val = HAL_I2C_Master_Transmit(hi2c1,
			MS5837_30BA_ADDRESS << 1,
			&command, 1,
			I2C_TIMEOUT_IN_MS);

	if (return_val != HAL_OK)
	{
		error_blink();
		return return_val; // If the command to read failed there is no point to receive
	}

	return HAL_I2C_Master_Receive(hi2c1,
			MS5837_30BA_ADDRESS << 1,
			data_ptr, MS5837_30BA_DATA_SIZE_IN_BYTES,
			I2C_TIMEOUT_IN_MS);
}

HAL_StatusTypeDef is_there_sensor_error(struct sensor_states* states_ptr)
{
	if (states_ptr->abs_pressure != HAL_OK ||
			states_ptr->diff_pressure != HAL_OK ||
			states_ptr->temperature != HAL_OK ||
			states_ptr->accel_gyro != HAL_OK)
	{
		error_blink();
		return HAL_ERROR;
	}

	return HAL_OK;
}

HAL_StatusTypeDef reinit_problem_sensors(struct sensor_states* states_ptr,
		struct sensor_coeffs* coeffs,
		I2C_HandleTypeDef* hi2c1)
{
	if (states_ptr == NULL || coeffs == NULL)
	{
		error_blink();
		return HAL_ERROR;
	}

	HAL_StatusTypeDef return_val = HAL_OK;
	HAL_StatusTypeDef temp_return_val;

	// Differential sensor doesn't need init
	if (states_ptr->diff_pressure != HAL_OK)
	{
		error_blink();
		return_val = states_ptr->diff_pressure;
		states_ptr = HAL_OK;
	}

	// Absolute sensor
	if (states_ptr->abs_pressure != HAL_OK)
	{
		temp_return_val = init_abs_pressure_sensor(coeffs, hi2c1);
		states_ptr->abs_pressure = temp_return_val;

		CHECK_RETURN_VAL(return_val, temp_return_val);
	}

	// Temperature sensor
	if (states_ptr->temperature != HAL_OK)
	{
		temp_return_val = init_temperature_sensor(coeffs, hi2c1);
		states_ptr->temperature = temp_return_val;

		CHECK_RETURN_VAL(return_val, temp_return_val);
	}

	// Accel and gyro
	if (states_ptr->accel_gyro != HAL_OK)
	{
		temp_return_val = init_accel_gyro_sensor(hi2c1); // This isn't likely to fix a problem
		states_ptr->accel_gyro = temp_return_val;

		CHECK_RETURN_VAL(return_val, temp_return_val);
	}

	return return_val;
}

uint16_t convert_twos_comp_to_dec(uint16_t in)
{
	return ~in + 1;
}

static HAL_StatusTypeDef check_accel_gyro_data(struct sensor_data* data_ptr)
{
	if (abs(data_ptr->accel_x) > LSM6DS3_MAX_ACCEL ||
		abs(data_ptr->accel_y) > LSM6DS3_MAX_ACCEL ||
		abs(data_ptr->accel_z) > LSM6DS3_MAX_ACCEL ||
		abs(data_ptr->gyro_x) > LSM6DS3_MAX_GYRO ||
		abs(data_ptr->gyro_y) > LSM6DS3_MAX_GYRO ||
		abs(data_ptr->gyro_z) > LSM6DS3_MAX_GYRO ||
		data_ptr->accel_gyro_temperature > LSM6DS3_MAX_TEMPERATURE ||
		data_ptr->accel_gyro_temperature < LSM6DS3_MAX_TEMPERATURE)
	{
		error_blink();
		return HAL_ERROR;
	}

	return HAL_OK;
}
