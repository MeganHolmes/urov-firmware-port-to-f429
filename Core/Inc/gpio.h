#ifndef __GPIO_H__
#define __GPIO_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"

#define CHECK_RETURN_VAL(rv, trv) if (trv != HAL_OK) \
		rv = trv

typedef enum
{
	RUDDER_DRIVE_TOGETHER = 0,
	RUDDER_DRIVE_DIFF = 1
} Rudder_Drive_Direction;

typedef enum
{
	Bouyancy_Direction_HOLD = 0,
	Bouyancy_Direction_UP = 1,
	Bouyancy_Direction_DOWN = 2
} Bouyancy_Direction;

struct commands
{
	uint16_t thruster;
	uint16_t elevator;
	uint16_t rudder;
	Rudder_Drive_Direction rudder_drive;
	Bouyancy_Direction bouyancy_control;
	Bouyancy_Direction last_bouyancy_control;
};
/* USER CODE END Private defines */
/* USER CODE BEGIN Prototypes */
void blinky(void);
void error_blink(void);
void green_blinky(void);

#ifdef __cplusplus
}
#endif
#endif /*__ GPIO_H__ */
