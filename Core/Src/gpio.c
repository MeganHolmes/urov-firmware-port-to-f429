#include "gpio.h"

#define BLINK_SPEED_IN_MS (0)

void blinky(void)
{
	HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
	HAL_Delay(BLINK_SPEED_IN_MS);
}

void green_blinky(void)
{
	HAL_GPIO_TogglePin(LD1_GPIO_Port, LD1_Pin);
	HAL_Delay(BLINK_SPEED_IN_MS);
}

void error_blink(void)
{
	HAL_GPIO_WritePin(LD3_GPIO_Port, LD3_Pin, GPIO_PIN_SET);
	HAL_Delay(BLINK_SPEED_IN_MS);
}
