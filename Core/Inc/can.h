#ifndef __FDCAN_H__
#define __FDCAN_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_can.h"

/* USER CODE BEGIN Includes */
#include "sensors.h"
#include "tim.h"

CAN_TxHeaderTypeDef setup_tx_header();
HAL_StatusTypeDef tx_data(struct sensor_data* data_ptr,
		struct sensor_states* states_ptr,
		CAN_TxHeaderTypeDef* tx_header,
		CAN_HandleTypeDef* hcan1);
HAL_StatusTypeDef rx_data(struct commands* cmds,
		CAN_HandleTypeDef* hcan1);
void send_error_code(uint16_t code,
		CAN_HandleTypeDef* hcan1);

#ifdef __cplusplus
}
#endif
#endif /*__ GPIO_H__ */
